Node-RED
===
"Flow-based programming for the Internet of Things"


# Launch
Already installed on the Raspberry Pi

1. Launch `node-red` in terminal.
2. Open [`localhost:1880`](localhost:1880) in browser.
3. If no flow is displayed, choose _Import -> Clipboard_ and import file _/home/pi/Documents/node-red/mqtt-test-flows.json_ (also available [here](https://gitlab.com/wimp.today/documentation/blob/master/mqtt-test-flows.json))
4. Check if Mosquitto is running by launching `mosquitto` in terminal.

# Flows

In this file, there are two flows:
- **Subscribe flow:** First node subscribes to the topic _TestTopic_ and prints the messages in the debug panel thanks to second node.
- **Publish flow:** First node creates an input message (by default, the timestamp) and the second node publishes it on the topic _TestTopic_.

# Test

To test the first flow, launch `mosquitto_pub -m "A message" -t TestTopic` in a terminal. The message should appear in the debug panel.

To test the two flows, click on the button of the input node in the _Publish flow_. The timstamp should appear in the debug panel.

# To do

- [x] MQTT broker
- [ ] Cloud integration (Kapua)
- [ ] [Security](https://nodered.org/docs/user-guide/runtime/securing-node-red)

# Resources

- [User guide](https://nodered.org/docs/user-guide/)
- [Node RED Programming Guide](http://noderedguide.com/) - Lectures with hands-on examples
- [Node-RED for smart home](https://www.linkedin.com/pulse/node-red-smart-home-making-your-first-rule-tutorial-artyom-syomushkin/) using openHAB 
- [Node-RED as a rule/script engine for openHAB](https://community.openhab.org/t/node-red-as-alternative-rule-engine/29509) (see [openhab.md](https://gitlab.com/wimp.today/documentation/blob/master/openhab.md))