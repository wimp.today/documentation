OpenHAB
===

[OpenHAB](https://www.openhab.org/) is a "a vendor and technology agnostic open source automation software for your home". It lets the user manage things and items, writes [rules](https://www.openhab.org/docs/configuration/rules-dsl.html) with a simple syntax, create dashboards ([sitemaps](https://www.openhab.org/docs/tutorial/uis.html#the-basic-ui) and [HABPanels](https://community.openhab.org/t/examples-of-habpanel-solutions/15557))...

- Docs: https://www.openhab.org/docs
- Examples: https://community.openhab.org/c/tutorials-examples

## Cloud integration

### OpenHAB Cloud

- [openHAB Cloud](https://github.com/openhab/openhab-cloud/blob/master/README.md)
- [openHAB Cloud add-on](https://www.openhab.org/addons/integrations/openhabcloud/)
- [myopenHAB](https://www.myopenhab.org/) : free instance to avoid hosting a personal instance

![OpenHAB Cloud Architecture](https://raw.githubusercontent.com/openhab/openhab-cloud/master/docs/FunctionalArchitecture_openHAB-cloud_v1.0.png)

### Other systems

- [Node-RED](https://community.openhab.org/t/node-red-as-alternative-rule-engine/29509)
- [IFTTT](https://ifttt.com/openhab)
- [Azure IoT Hub Connector](https://www.openhab.org/addons/integrations/azureiothub/)
- [Google Calendar Scheduler](https://www.openhab.org/addons/integrations/gcal/)

As far as I know, there does not seem to be an integration solution for Kapua or Kura.

## Advantages

- Open source solution
- Supports many [technologies and devices](https://www.openhab.org/addons/)
- Seems easy to use
- Rules in a language based on Java
- Server runs on Linux, Windows, Rasperry Pi...

## Limits

- Good for home automation, but no scalability?
- Security? (Maybe good)
