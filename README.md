# Documentation

This repository is to store all the information from any project related to WIMP.

You can also follow the project at https://wimp.today/.

The test calendar is https://calendar.google.com/calendar/ical/wimp.today%40gmail.com/public/basic.ics

[Sensors and Hardware](sensors.md)